#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int R_backup(char* path,char* work_directory,int backup){
	chdir(work_directory);
	int fd1 = open("last_number",O_RDONLY);
	char sn[100];
	read(fd1,sn,100);
	close(fd1);
	
	int nl = atoi(sn);
	int i;
	
	char file1[] = "last";
	char file2[] = "last1";
	int pid = fork();
	char com1[] = "/bin/cp";
	if (!pid){
		char* c[4] = {NULL};
		c[0] = com1;
		c[1] = file1;
		c[2] = file2;
		execv(c[0],c);
	} else {
		int a;
		waitpid(-1,&a,0);
	}

	char ln1[100];
	for (i = nl - 1;i >= backup;i--){
		sprintf(ln1,"%d",i);
		if (open(ln1,O_RDONLY) != -1){
			int pid1 = fork();
			if (!pid1){
				freopen(ln1,"r",stdin);
				char* c1[4] = {NULL};
				char com2[] = "/usr/bin/patch";
				char com3[] = "-R";
				c1[0] = com2;
				c1[1] = com3;
				execv(c1[0],c1);
			} else {
				int a1;
				waitpid(-1,&a1,0);
			}
		}
	}

	freopen("/dev/tty","r",stdin);
	
	pid = fork();
	char com4[] = "/bin/mv";
	if (!pid){
		char* c2[4] = {NULL};
		c2[0] = com4;
		c2[1] = file1;
		c2[2] = path;
		execv(c2[0],c2);
	} else {
		int a2;
		waitpid(-1,&a2,0);
	}
	
	pid = fork();
	if (!pid){
		char* c3[4] = {NULL};
		c3[0] = com4;
		c3[1] = file2;
		c3[2] = file1;
		execv(c3[0],c3);
	} else {
		int a3;
		waitpid(-1,&a3,0);
	}
	return 0;
}

int main(int argv,char** argc){
	R_backup(argc[1],argc[2],atoi(argc[3]));
}
