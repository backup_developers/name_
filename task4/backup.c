#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int Create_file_last_number(int backup){
	char file2[] = "last_number";
	int fd1 = open(file2,O_CREAT,0777);
	close(fd1);
	fd1 = open(file2,O_WRONLY);
	char ln[100];
	sprintf(ln,"%d",backup);
	write(fd1,ln,strlen(ln));
	close(fd1);	
	return 0;
}

int Create_file(char* filename,char* foldername,int backup){
//filename - absolute path of file with i work, foldername - work folder,which i change with change_dir, backup - number of backup
//I write backup in file number_last and copy file filename to foldername
	char file1[] = "last";
	char file2[] = "last_number";
	
	Create_file_last_number(backup);

	int pid = fork();
	char com1[] = "/bin/cp";
	if (!pid){
		char* c[4] = {NULL};
		c[0] = com1;
		c[1] = filename;
		c[2] = file1;
		execv(c[0],c);
	} else {
		int a;
		waitpid(-1,&a,0);
	}
	
	return 0;	
}

int Change_file(char* filename,char* foldername,int backup){
	char file1[] = "last";
	char file2[] = "last_number";
//creat file "time" and read backup number	
	int fd1 = open(file2,O_RDONLY);
	int fd2 = open("time",O_CREAT,0777);
	close(fd2);
	char sn[100];
	read(fd1,sn,100);
	close(fd1);

//creat file diff
	int pid = fork();
	FILE* F1;
	if (!pid){
		F1 = freopen(sn,"w",stdout);
		char com1[] = "/usr/bin/diff";
		char com2[] = "-u";
		char* c[7] = {NULL};
		c[0] = com1;
		c[1] = com2;
		c[2] = file1;
		c[3] = filename;
		execv(c[0],c);
		printf("function diff not doing\n");
	} else {
		int a;
		waitpid(-1,&a,0);
	}
	//fclose(F1);
	freopen("/dev/tty","w",stdout);
//rewrite "last_number"
	unlink(file2);
	Create_file_last_number(backup);
	
//patch file "last"
	pid = fork();
	FILE* F2;
	if (!pid){
		F1 = freopen(sn,"r",stdin);
		F2 = freopen("time","w",stdout);
		char* c1[2] = {NULL};
		char com3[] = "/usr/bin/patch";
		c1[0] = com3;
		execv(c1[0],c1);
		printf("function patch hasn't done\n");
	} else {
		int a1;
		waitpid(-1,&a1,0);
	}
	//fclose(F2);
	//fclose(F1);
	freopen("/dev/tty","r",stdin);
	freopen("/dev/tty","w",stdout);
	unlink("time");
	return 0;
}

int Backup_file(char* filename,char* foldername,int backup){
	int k = chdir(foldername);
	if (k == -1){
		printf("Error change working directory\n");
		return 1;
	}
	int s = open("last_number",O_RDONLY);
	if (s == -1){
		Create_file(filename,foldername,backup);
	} else {
		close(s);
		Change_file(filename,foldername,backup);
	}
	return 0;
}

int main(int argc,char** argv){
	Backup_file(argv[1],argv[2],atoi(argv[3]));
	return 0;
}
