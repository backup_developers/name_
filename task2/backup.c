#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/wait.h>
void fat_error(char * str) {
	printf("ERROR: [%s] ERRNO:[%d][%s] ", str, errno, strerror(errno));
	exit(255);
}
void backup_dir(char * name_in_dir, char * name_out_dir) {
	assert(name_in_dir);
	assert(name_out_dir);
	
	printf("current_dir:[%s]\n", name_in_dir);
	
	if (mkdir(name_out_dir, 0777) != 0){
		if (errno == EEXIST){
			errno = 0;
		}
		else 
			fat_error("Create dir error");
	}
	DIR* dir = opendir(name_in_dir);
	if (dir == NULL)
		fat_error("opendir has broken");
	
	while (1) {
		errno = 0;
		struct dirent *file = readdir(dir);
		if ((file == NULL) && (errno != 0)) {
			printf("dir: [%s]\n", name_in_dir);
			fat_error("readdir has broken");
		}
		if (file == NULL)
			break;
		
		char in_name[PATH_MAX] = {};
		char out_name[PATH_MAX] = {};
		sprintf(in_name, "%s/%s", name_in_dir, file->d_name);
		sprintf(out_name, "%s/%s", name_out_dir, file->d_name);
		
		if (strcmp(file->d_name, ".") && (strcmp(file->d_name, ".."))) {
			printf("\t Current file:[%s]", file->d_name);
			if (file->d_type == DT_DIR) {
				printf("..backup dir..\n");
				backup_dir(in_name, out_name);
			} 
		}
	}
	if (closedir(dir) != 0)
		fat_error("closedir failed");
	printf("Out dirrectory\n");
}
int main(int argc, char* argv[]){
if (argc != 3) {
	printf("Bad arguments!\n");
	return 0;
}
char * name_in_dir = argv[1];
char * name_out_dir = argv[2];
char * pr = strrchr(name_in_dir, '/');
pr = pr + 1;
strcat(name_out_dir,"/backup_");
strcat(name_out_dir, pr);
backup_dir(name_in_dir, name_out_dir);
return 0;
}
