#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
int backup_date(char* work_dir,int backup){
	chdir(work_dir);
	int fd = open(".backupdat",O_RDONLY);
	if (fd == -1){
		fd = open(".backupdat",O_CREAT,0777);
		close(fd);
		fd = open(".backupdat",O_WRONLY);
	}
	char c[100];
	sprintf(c,"%d ",backup);
	lseek(fd,SEEK_END,0);
	write(fd,c,strlen(c));
	close(fd);

	int pid = fork();
	int a;
	if (!pid){
		freopen(".backupdat","w",stdout);
		char com[]  = "/bin/date";
		char* c[2] = {NULL};
		c[0] = com;
		execv(c[0],c);
	} else {
		waitpid(-1,&a,0);
	}
	freopen("/dev/tty","w",stdout);
	return 0;		
}
int main(int argv,char** argc){
	backup_date(argc[1],atoi(argc[2]));
}
